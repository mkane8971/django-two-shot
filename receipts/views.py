from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, CreateCategory, CreateAccount
# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipt_list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            purchaser = request.user
            receipt = form.save(False)
            receipt.purchaser = purchaser
            receipt.save()
        return redirect('home')
    else:
        form = CreateReceipt()
    context = {
        "form": form,
    }
    return render(request,"receipts/create_receipt.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_list = []
    for category in categories:
        num_receipts = Receipt.objects.filter(category=category).count()
        category_list.append({
            'name':category.name,
            'num_receipts': num_receipts
        })
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CreateCategory()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html" , context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_list = []
    for account in accounts:
        num_receipts = Receipt.objects.filter(account=account).count()
        account_list.append({
            'name': account.name,
            'number': account.number,
            'num_receipts': num_receipts
        })
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccount()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)
